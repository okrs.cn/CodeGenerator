package com.mmk.code.common;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class CodeSaveTool {

	/**
	 * 默认方式保存
	 * @param projectPath
	 * @param packagePath
	 * @param javaname
	 * @param code
	 * @throws IOException
	 */
	public static void save(String projectPath,String packagePath,String javaname,String code ) throws IOException{
		String pp = packagePath.replace(".", File.separator);
		StringBuilder path = new StringBuilder(projectPath);
		if(!projectPath.endsWith(File.separator)){
			path.append(File.separator);
		}
		path.append(pp);
		String pathVar = path.toString();
		if(Files.notExists(Paths.get(pathVar ))){
			Files.createDirectories(Paths.get(pathVar));
		}
		Files.deleteIfExists(Paths.get(pathVar+File.separator+javaname));
		Files.write(Paths.get(pathVar+File.separator+javaname), code.getBytes(),StandardOpenOption.CREATE);
	}

	/**默认方式保存
	 * 保存java代码部分
	 * @param path 保存路径
	 * @param packagePath 包路径
	 * @param name 文件名 
	 * @param code 代码内容
	 * @throws IOException IO异常 
	 */
	public static void saveCode(String path, String packagePath, String name, String code) throws IOException {
		save(path+"/src/main/java",packagePath,name,code);
	}

	/**默认方式保存
	 * 保存页面模板
	 * @param path 保存路径
	 * @param packagePath 包路径
	 * @param name 文件名 
	 * @param code 代码内容
	 * @throws IOException IO异常 
	 */
	public static void saveTemplates(String path, String packagePath, String name, String code) throws IOException {
		save(path+"/src/main/webapp/WEB-INF/views",packagePath,name,code);
	}
	
	/**
	 * 默认方式保存
	 * @param path
	 * @param packagePath
	 * @param name
	 * @param code
	 * @throws IOException
	 */
	public static void saveJs(String path, String packagePath, String name, String code) throws IOException {
		save(path+"/src/main/resources/static/js",packagePath,name,code);
	}
	
	
	
	/**
	 * 自定义方式保存
	 * @param projectPath
	 * @param packagePath
	 * @param javaname
	 * @param code
	 * @throws IOException
	 */
	public static void saveDefined(String projectPath,String packagePathName,String code ) throws IOException{
		String packagepath=packagePathName.substring(0, packagePathName.lastIndexOf(".")); 
		String packagepath2=packagePathName.substring(0, packagepath.lastIndexOf("."));
		String  javaname=packagePathName.substring(packagepath.lastIndexOf(".")+1);
		 
		String pp = packagepath2.replace(".", File.separator);
		StringBuilder path = new StringBuilder(projectPath);
		if(!projectPath.endsWith(File.separator)){
			path.append(File.separator);
		}
		path.append(pp);
		String pathVar = path.toString();
		if(Files.notExists(Paths.get(pathVar ))){
			Files.createDirectories(Paths.get(pathVar));
		}
		Files.deleteIfExists(Paths.get(pathVar+File.separator+javaname));
		Files.write(Paths.get(pathVar+File.separator+javaname), code.getBytes(),StandardOpenOption.CREATE);
	}

	/**自定义方式保存
	 * 保存java代码部分
	 * @param path 保存路径
	 * @param packagePath 包路径
	 * @param name 文件名 
	 * @param code 代码内容
	 * @throws IOException IO异常 
	 */
	public static void saveCodeDefined(String path, String packagePathName, String code) throws IOException {
		saveDefined(path,packagePathName,code);
	}

	/**自定义方式保存
	 * 保存页面模板
	 * @param path 保存路径
	 * @param packagePath 包路径
	 * @param name 文件名 
	 * @param code 代码内容
	 * @throws IOException IO异常 
	 */
	public static void saveTemplatesDefined(String path, String packagePathName, String code) throws IOException {
		saveDefined(path,packagePathName,code);
	}
	
	/**
	 * 自定义方式保存
	 * @param path
	 * @param packagePath
	 * @param name
	 * @param code
	 * @throws IOException
	 */
	public static void saveJsDefined(String path, String packagePathName, String code) throws IOException {
		saveDefined(path,packagePathName,code);
	}
	
	 
}
