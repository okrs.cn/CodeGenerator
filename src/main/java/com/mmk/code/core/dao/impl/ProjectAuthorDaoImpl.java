package com.mmk.code.core.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.mmk.code.core.condition.ProjectCondition;
import com.mmk.code.core.dao.ProjectAuthorDao;
import com.mmk.code.core.dao.ProjectDao;
import com.mmk.code.core.model.Project;
import com.mmk.code.core.model.ProjectAuthor;
import com.mmk.gene.dao.impl.SpringDataQueryDaoImpl;



/**
*@Title: ProjectAuthorDaoImpl
*@Description: 项目 数据持久层接口实现
*@author code generator
*@version 1.0
*@date 2016-07-19 14:16:56
*/
@Repository
public class ProjectAuthorDaoImpl extends SpringDataQueryDaoImpl<ProjectAuthor> implements ProjectAuthorDao {
    
    private Log log = LogFactory.getLog(this.getClass());
    
    public ProjectAuthorDaoImpl(){
        super(ProjectAuthor.class);
    }
     
    
    @Override 
    public ProjectAuthor findBy(String field,Object value){
        StringBuffer sb=new StringBuffer("select model from ProjectAuthor model  where model.");
        sb.append(field);
        sb.append(" = :value ");
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("value",value);
        List<ProjectAuthor> result = queryByJpql(sb.toString(), params,0l,1l);
        return result.isEmpty() ? null : result.get(0);
    }
 
    @Override 
    public ProjectAuthor findByAuthorAndProjectId(String author,Object projectId){
        StringBuffer sb=new StringBuffer("select model from ProjectAuthor model  where model.author= :author and model.projectId= :projectId ");
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("author",author);
        params.put("projectId",projectId);
        List<ProjectAuthor> result = queryByJpql(sb.toString(), params,0l,1l);
        return result.isEmpty() ? null : result.get(0);
    }
    
}