package com.mmk.code.core.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;

/**
*@Title: Project
*@Description: 项目 作者信息
*@author code generator
*@version 1.0
*@date 2016-07-19 14:16:55
*/
@Entity
@Table(name = "project_author")
public class ProjectAuthor {
    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    /**
     * 项目ID
     */
    @Column(name="project_id")
    private Long projectId;

    /**
     * 生成路径
     */
    @Column(name="path")
    private String path;

    /**
     * 作者
     */
    @Column(name="author")
    private String author;
    
    /**
     * 解锁码
     */
    @Column(name="password")
    private String password;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
 
}