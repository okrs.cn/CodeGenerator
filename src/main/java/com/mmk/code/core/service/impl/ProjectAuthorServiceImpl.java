package com.mmk.code.core.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mmk.code.core.condition.ProjectCondition;
import com.mmk.code.core.dao.ProjectAuthorDao;
import com.mmk.code.core.dao.ProjectAuthorRepository;
import com.mmk.code.core.dao.ProjectDao;
import com.mmk.code.core.dao.ProjectRepository;
import com.mmk.code.core.model.Project;
import com.mmk.code.core.model.ProjectAuthor;
import com.mmk.code.core.service.ProjectAuthorService;
import com.mmk.code.core.service.ProjectService;
import com.mmk.gene.service.impl.BaseServiceImpl;
/**
*@Title: ProjectAuthorServiceImpl
*@Description: 项目 业务服务层实现
*@author code generator
*@version 1.0
*@date 2016-07-19 14:16:56
*/
@Service
public class ProjectAuthorServiceImpl extends BaseServiceImpl<ProjectAuthor, Long> implements ProjectAuthorService {

    private Log log = LogFactory.getLog(this.getClass());
    @Resource
    private ProjectAuthorDao projectAuthoDao;
    
    private ProjectAuthorRepository projectAuthoRepository;
    /**
    *构造方法
    */
    @Autowired
    public ProjectAuthorServiceImpl( ProjectAuthorRepository projectAuthorRepository) {
        super(projectAuthorRepository);
        this.projectAuthoRepository = projectAuthorRepository;
    }
 
    @Override 
    public ProjectAuthor findBy(String field,Object value){
        log.info("项目根据字["+field+"="+value+"] 进行查询符合条件的唯一值");
        return projectAuthoDao.findBy(field,value);
    }
    
    @Override 
    public ProjectAuthor findByAuthorAndProjectId(String author,Object projectId){
        log.info("项目根据["+author+","+projectId+"] 进行查询符合条件的唯一值");
        return projectAuthoDao.findByAuthorAndProjectId(author,projectId);
    }
    
     
}