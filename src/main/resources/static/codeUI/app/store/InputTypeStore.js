/**
 * 编辑页面--字段类型
 */Ext.define('Code.store.InputTypeStore', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.field.Field'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'InputTypeStore',
            data: [
				{
				    inputType: 'none'
				},
                {
                    inputType: 'input'
                },
                {
                    inputType: 'hidden'
                },
                {
                    inputType: 'time'
                },
                {
                    inputType: 'date'
                },
                {
                    inputType: 'datetime'
                },
                {
                    showType: 'outusername'
                },
                {
                    inputType: 'textarea'
                },
                {
                    inputType: 'checkbox'
                },
                {
                    inputType: 'radio'
                },
                {
                    inputType: 'select'
                },
                {
                    inputType: 'htmleditor'
                },
                {
                    inputType: 'file'
                },
                {
                    inputType: 'files'
                },
                {
                    inputType: 'onetoone'
                },
                {
                    inputType: 'onetomanylist'
                }
				,
                {
                    inputType: 'onetomanyset'
                }
				,
                {
                    inputType: 'onetomanymap'
                }
				,
                {
                    inputType: 'manytomanylist'
                }
				,
                {
                    inputType: 'manytomanyset'
                }
				,
                {
                    inputType: 'manytomanymap'
                }
            ],
            fields: [
                {
                    name: 'inputType'
                }
            ]
        }, cfg)]);
    }
});