Ext.define('Code.store.ColumnStore', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.field.Field'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'ColumnStore',
            proxy: {
                type: 'ajax',
                url: 'http://localhost:10001/field/loadByModelId',
                reader: {
                    type: 'json'
                }
            },
            fields: [
                {
                    name: 'isPk'
                },
                {
                    name: 'columnName'
                },
                {
                    name: 'DATA_TYPE'
                },
                {
                    name: 'nullable'
                },
                {
                    name: 'autoincrement'
                },
                {
                    name: 'length'
                },
                {
                    name: 'comment',
                    validators: [
                        {
                            type: 'length',
                            field: 'COMMENTS',
                            max: 100,
                            min: 1
                        },
                        {
                            type: 'presence',
                            field: 'COMMENTS'
                        }
                    ]
                },
                {
                    name: 'type'
                },
                {
                    name: 'field'
                },
                {
                    name: 'existsMethod'
                },
                {
                    name: 'findBy'
                },
                {
                    name: 'findAllBy'
                },
                {
                    name: 'matchType'
                },
                {
                    name: 'listShow'
                },
                {
                    name: 'validate'
                },
                {
                    name: 'inputType'
                },
                {
                    name: 'showType'
                },
                {
                    name: 'id'
                }
            ]
        }, cfg)]);
    }
});